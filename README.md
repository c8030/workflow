# Microservice Event Based Orchestrator Using Camunda

This demo consists of 2 services: Order Service and Inventory Service. Both of these services are based on hexagonal architecture where business logic can be accessed through multiple interfaces such as rest api or commands through messaging framework. For this poc i have chosen messaging interface.


# Use Case

Use case itself is really simple where user places a request for order. Before placing order inventory is reserved. once inventory is reserved then orders is created. and once payment is done order is confirmed. if for some reason order confirmation fails then whole transaction has to be rolled back. here it means we need to cancel the order and then release the inventory.

## Deployment Architecture

Camunda Engine is deployed separately and Spring boot service acts as a worker for the commands produced by workflow instances. This approch is scalable because the only task camunda is doing is publishing messages.

## Steps to Run
* Deploy order-workflow.bpmn in camunda engine
* Build Codex-Core
* Build and Run Inventory Service. Camunda rest path has to be specified in application.yml.
* Build and Run Order Service  service. Camunda rest path has to be specified in application.yml.
* Start the camunda workflow with order json in variable named 'payload'.
